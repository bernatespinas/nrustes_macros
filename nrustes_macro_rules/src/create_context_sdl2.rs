#[macro_export]
macro_rules! create_context_sdl2 {
	(
		$context:ident,
		$texture_creator:ident,
		nrustes_config: $nrustes_config:ident,
		window_title: $window_title:literal,
		window_mode: $window_mode:literal
	) => {
		// Because of SDL2's Font (lifetimes, borrows of ttf_context...), I can't create the font in a create method like I do with other Contexts. So, I create it here instead.
		let ttf_context = nrustes::backends::ContextSdl2::sdl2_ttf_context();

		let ttf_font_file_path = $nrustes_config.ttf_font_file_path.as_ref().expect("Missing \"ttf_font_file_path\" in nrustes config file.");
		
		// TODO Where does that 64 come from? With 14 it looks bad, but with 64 it looks crisp.
		let font = ttf_context
			.load_font(ttf_font_file_path, 64)
			// .set_style(sdl2::ttf::FontStyle::NORMAL)
			.expect("Font loading failed.");

		let sdl2_context =
			nrustes::backends::ContextSdl2::sdl2_context().expect("SDL2 context creation failed.");

		let event_pump = sdl2_context
			.event_pump()
			.expect("SDL2 event pump creation failed.");

		let event_subsystem = sdl2_context
			.event()
			.expect("SDL2 event subsystem creation failed.");

		let video_subsystem = sdl2_context
			.video()
			.expect("SDL2 video subsystem creation failed.");

		let window = match $window_mode {
			"fullscreen" => {
				let display_mode = video_subsystem.current_display_mode(0).unwrap();
				let width = display_mode.w as u32;
				let height = display_mode.h as u32;

				video_subsystem
					.window($window_title, width, height)
					.fullscreen()
					.opengl()
					.build()
					.expect("SDL window creation failed.")
			},
			"fullscreen_windowed" => {
				let display_mode = video_subsystem.current_display_mode(0).unwrap();
				let width = display_mode.w as u32;
				let height = display_mode.h as u32;

				video_subsystem
					.window($window_title, width, height)
					.borderless()
					.maximized()
					.opengl()
					.build()
					.expect("SDL window creation failed.")
			},
			"windowed" => {
				log::warn!("Window mode \"windowed\" does not fully work as expected!");
				println!("Window mode \"windowed\" does not fully work as expected!");

				let display_usable_bounds = video_subsystem
					.display_usable_bounds(0)
					.expect("display_usable_bounds failed.");

				let width = display_usable_bounds.width();
				let height = display_usable_bounds.height();

				video_subsystem
					.window($window_title, width, height)
					.opengl()
					.build()
					.expect("SDL window creation failed.")
			},
			_ => panic!("Invalid window mode: it has to be either \"fullscreen\", \"fullscreen_windowed\" or \"windowed\""),
		};

		let canvas = window
			.into_canvas()
			.target_texture()
			.present_vsync()
			.accelerated()
			.build()
			.expect("SDL canvas creation failed.");

		let $texture_creator = canvas.texture_creator();

		let mut $context = nrustes::backends::ContextSdl2::new(
			font,
			event_pump,
			event_subsystem,
			canvas,
			$nrustes_config,
		);
	};
}
