use proc_macro::TokenStream;
use quote::quote;
use syn::{Data, DataStruct, DeriveInput, Fields};

use crate::{contains_field, fields_with_attr, FieldsComma};

fn draw_quotes(fields: &FieldsComma) -> proc_macro2::TokenStream {
	let draw_fields: Vec<_> = fields_with_attr(fields, "draw").collect();

	if draw_fields.is_empty() {
		panic!("There must be at least one field with the `#[draw]` attribute.");
	}

	draw_fields
		.into_iter()
		.map(|field| {
			let field_name = field.ident.as_ref().expect("missing ident in field");

			quote! {
				self.#field_name.draw(context);
			}
		})
		.collect()
}

fn darken_quotes(fields: &FieldsComma) -> proc_macro2::TokenStream {
	fields_with_attr(fields, "draw")
		.map(|field| {
			let field_name = field.ident.as_ref().expect("missing ident in field");

			quote! {
				self.#field_name.set_darkened(darkened);
			}
		})
		.collect()
}

fn needs_draw_quotes(fields: &FieldsComma) -> proc_macro2::TokenStream {
	let fields_need_draw: proc_macro2::TokenStream = fields_with_attr(fields, "draw")
		.map(|field| {
			let field_name = field.ident.as_ref().expect("missing ident in field");

			quote! {
				self.#field_name.needs_draw() ||
			}
		})
		// TODO Use of unstable feature.
		// .intersperse(quote! {
		// 	||
		// })
		// TODO Workaround which does not work.
		// .collect::<Vec<_>>()
		// .join(quote! {
		// ||
		// })
		.collect();

	if contains_field(fields, "needs_draw") {
		quote! {
			#fields_need_draw self.needs_draw
		}
	} else {
		quote! {
			#fields_need_draw false
		}
	}
}

fn schedule_draw_quotes(fields: &FieldsComma) -> proc_macro2::TokenStream {
	let fields_schedule_draw: proc_macro2::TokenStream = fields_with_attr(fields, "draw")
		.map(|field| {
			let field_name = field.ident.as_ref().expect("missing ident in field");

			quote! {
				self.#field_name.schedule_draw();
			}
		})
		.collect();

	if contains_field(fields, "needs_draw") {
		quote! {
			#fields_schedule_draw
			self.needs_draw = true;
		}
	} else {
		fields_schedule_draw
	}
}

pub fn impl_draw_macro(input: &DeriveInput) -> TokenStream {
	let st_name = &input.ident;
	let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

	let fields = match &input.data {
		Data::Struct(DataStruct {
			fields: Fields::Named(named_fields),
			..
		}) => &named_fields.named,
		_ => panic!("Only named fields are supported."),
	};

	let draw_quotes = draw_quotes(fields);
	let darken_quotes = darken_quotes(fields);

	let needs_draw_quotes = needs_draw_quotes(fields);

	let schedule_draw_quotes = schedule_draw_quotes(fields);

	let gen = quote! {
		#[automatically_derived]
		impl #impl_generics Draw for #st_name #ty_generics #where_clause {
			fn draw(&mut self, context: &mut impl Context) {
				#draw_quotes
			}

			fn needs_draw(&self) -> bool {
				#needs_draw_quotes
			}

			fn schedule_draw(&mut self) {
				#schedule_draw_quotes
			}
		}

		#[automatically_derived]
		impl #impl_generics Darken for #st_name #ty_generics #where_clause {
			fn set_darkened(&mut self, darkened: bool) {
				#darken_quotes
				self.schedule_draw();
			}
		}
	};

	gen.into()
}
