use proc_macro::TokenStream;
use quote::quote;
use syn::{Data, DataStruct, DeriveInput, Fields};

mod double_view_state_attribute;
use double_view_state_attribute::DoubleViewStateData;

pub fn derive_double_view_holder(input: &DeriveInput) -> TokenStream {
	let st_name = &input.ident;
	let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

	let fields = match &input.data {
		Data::Struct(DataStruct {
			fields: Fields::Named(named_fields),
			..
		}) => &named_fields.named,
		_ => panic!("Only named fields are supported."),
	};

	let DoubleViewStateData {
		double_view_state_ident,
		main_container_type,
		secondary_container_type,
		shortcuts_data_type,
	} = DoubleViewStateData::new(fields, "draw");

	let gen = quote! {
		#[automatically_derived]
		impl #impl_generics DoubleViewHolder for #st_name #ty_generics #where_clause {
			type Container1 = #main_container_type;
			type Container2 = #secondary_container_type;
			type Action = #shortcuts_data_type;

			fn view_state(
				&self,
			) -> &DoubleViewState<Self::Container1, Self::Container2, Self::Action> {
				&self.#double_view_state_ident
			}

			fn view_state_mut(
				&mut self,
			) -> &mut DoubleViewState<Self::Container1, Self::Container2, Self::Action> {
				&mut self.#double_view_state_ident
			}
		}
	};

	gen.into()
}
