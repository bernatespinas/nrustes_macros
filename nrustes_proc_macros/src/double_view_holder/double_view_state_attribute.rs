use syn::{GenericArgument, Ident, PathArguments, Type};

use crate::{fields_with_attr, FieldsComma};

pub struct DoubleViewStateData {
	pub double_view_state_ident: Ident,
	pub main_container_type: Type,
	pub secondary_container_type: Type,
	pub shortcuts_data_type: Type,
}

impl DoubleViewStateData {
	pub fn new(fields: &FieldsComma, attr: &'static str) -> Self {
		let double_view_state_fields: Vec<_> = fields_with_attr(fields, attr).collect();

		if double_view_state_fields.len() != 1 {
			panic!("There has to be one, and only one, {attr} field.");
		}

		let double_view_state = double_view_state_fields.first().unwrap();

		match &double_view_state.ty {
			Type::Path(path) => {
				let path_arguments = &path.path.segments.first().unwrap().arguments;

				match path_arguments {
					PathArguments::AngleBracketed(arguments) => {
						if arguments.args.len() != 3 {
							panic!("{attr} has to be of type DoubleViewState.");
						}

						let mut args = arguments.clone().args.into_iter();

						let GenericArgument::Type(main_container_type) = args.next().unwrap() else {
							panic!("{attr} has to be of type DoubleViewState.");
						};

						let GenericArgument::Type(secondary_container_type) = args.next().unwrap() else {
							panic!("{attr} has to be of type DoubleViewState.");

						};

						let GenericArgument::Type(shortcuts_data_type) = args.next().unwrap() else {
							panic!("{attr} has to be of type DoubleViewState.");
						};

						Self {
							double_view_state_ident: double_view_state.ident.clone().unwrap(),
							main_container_type,
							secondary_container_type,
							shortcuts_data_type,
						}
					}
					_ => panic!("{attr} has to be of type DoubleViewState."),
				}
			}
			_ => panic!("{attr} has to be of type DoubleViewState."),
		}
	}
}
