use proc_macro::TokenStream;
use quote::quote;
use syn::{Data, DataStruct, DeriveInput, Fields};

mod container_attribute;
use container_attribute::ContainerAttributeData;

pub fn derive_mono_view_holder(input: &DeriveInput) -> TokenStream {
	let st_name = &input.ident;
	let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

	let fields = match &input.data {
		Data::Struct(DataStruct {
			fields: Fields::Named(named_fields),
			..
		}) => &named_fields.named,
		_ => panic!("TODO I can only work with named fields!"),
	};

	let ContainerAttributeData {
		container_ident: main_container_ident,
		container_type: main_container_type,
		shortcuts_action_type: main_container_action,
	} = ContainerAttributeData::new(fields, "main_container");

	let gen = quote! {
		#[automatically_derived]
		impl #impl_generics MonoViewHolder for #st_name #ty_generics #where_clause {
			type Container1 = #main_container_type;
			type Action = #main_container_action;

			fn main_container(&self) -> &Shortcuts<Self::Container1, Self::Action> {
				&self.#main_container_ident
			}

			fn main_container_mut(&mut self) -> &mut Shortcuts<Self::Container1, Self::Action> {
				&mut self.#main_container_ident
			}
		}
	};

	gen.into()
}
