use syn::{GenericArgument, Ident, PathArguments, Type};

use crate::{fields_with_attr, FieldsComma};

pub struct ContainerAttributeData {
	pub container_ident: Ident,
	pub container_type: Type,
	pub shortcuts_action_type: Type,
}

impl ContainerAttributeData {
	pub fn new(fields: &FieldsComma, container_attr: &'static str) -> Self {
		let container_fields: Vec<_> = fields_with_attr(fields, container_attr).collect();

		if container_fields.len() != 1 {
			panic!("There has to be one, and only one, {container_attr} field.");
		}

		let container_field = container_fields.first().unwrap();

		let (container_type, shortcuts_action_type) = match &container_field.ty {
			Type::Path(path) => {
				let path_arguments = &path.path.segments.first().unwrap().arguments;

				match path_arguments {
					PathArguments::AngleBracketed(arguments) => {
						if arguments.args.len() != 2 {
							panic!("{container_attr} has to be of type Shortcuts.");
						}

						let GenericArgument::Type(container_type) = arguments.args.first().unwrap() else {
							panic!("{container_attr} has to be of type Shortcuts.");
					};

						let GenericArgument::Type(shortcuts_action_type) = arguments.args.last().unwrap() else {
							panic!("{container_attr} has to be of type Shortcuts.");
					};

						(container_type.clone(), shortcuts_action_type.clone())
					}
					_ => panic!("{container_attr} has to be of type Shortcuts."),
				}
			}
			_ => panic!("{container_attr} has to be of type Shortcuts."),
		};

		Self {
			container_ident: container_field.ident.clone().unwrap(),
			container_type,
			shortcuts_action_type,
		}
	}
}
