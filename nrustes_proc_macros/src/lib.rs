use proc_macro::TokenStream;
use syn::DeriveInput;

mod derive_draw;
mod double_view_holder;
mod mono_view_holder;

/// A list of fields separated by a comma.
type FieldsComma = syn::punctuated::Punctuated<syn::Field, syn::token::Comma>;

#[proc_macro_derive(Draw, attributes(draw))]
pub fn derive_draw(input: TokenStream) -> TokenStream {
	let ast: DeriveInput = syn::parse(input).unwrap();

	derive_draw::impl_draw_macro(&ast)
}

#[proc_macro_derive(MonoViewHolder, attributes(main_container))]
pub fn derive_mono_view_holder(input: TokenStream) -> TokenStream {
	let ast: DeriveInput = syn::parse(input).unwrap();

	mono_view_holder::derive_mono_view_holder(&ast)
}

#[proc_macro_derive(DoubleViewHolder, attributes(main_container, secondary_container))]
pub fn derive_double_view_holder(input: TokenStream) -> TokenStream {
	let ast: DeriveInput = syn::parse(input).unwrap();

	double_view_holder::derive_double_view_holder(&ast)
}

/// Returns an `Iterator` of `syn::Field`s which have a specific attribute.
fn fields_with_attr<'a, I>(
	fields: &'a FieldsComma,
	attr_ident: &'static I,
) -> impl Iterator<Item = &'a syn::Field>
where
	I: ?Sized,
	proc_macro2::Ident: PartialEq<I>,
{
	fields.iter().filter(|field| {
		field
			.attrs
			.iter()
			.find(|attr| attr.path().is_ident(attr_ident))
			.is_some()
	})
}

fn contains_field<'a, I>(fields: &'a FieldsComma, field_ident: &'static I) -> bool
where
	I: ?Sized,
	proc_macro2::Ident: PartialEq<I>,
{
	fields
		.iter()
		.find(|field| field.ident.as_ref().unwrap() == field_ident)
		.is_some()
}
